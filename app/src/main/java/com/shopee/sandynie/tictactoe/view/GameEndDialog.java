package com.shopee.sandynie.tictactoe.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.shopee.sandynie.tictactoe.R;

public class GameEndDialog extends DialogFragment {

    private View rootView;
    private GameActivity activity;
    private String winnerName;

    public static GameEndDialog newInstance(GameActivity activity,String winnerName){
        GameEndDialog dialog = new GameEndDialog();
        dialog.activity = activity;
        dialog.winnerName = winnerName;
        return dialog;
    }

   public class PositiveButtonListener implements DialogInterface.OnClickListener {
       @Override
       public void onClick(DialogInterface dialog, int which) {
           dismiss();
           activity.promptForPlayers();
       }
   }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        initView();
        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setView(rootView)
                .setCancelable(false)
                .setPositiveButton(R.string.done,new PositiveButtonListener())
                .create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        return dialog;
    }

    private void initView(){
        rootView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_game_end,null,false);
        ((TextView)rootView.findViewById(R.id.tv_winner)).setText(winnerName);
    }
}
