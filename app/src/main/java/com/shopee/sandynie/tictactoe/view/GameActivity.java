package com.shopee.sandynie.tictactoe.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.shopee.sandynie.tictactoe.R;
import com.shopee.sandynie.tictactoe.databinding.ActivityGameBinding;
import com.shopee.sandynie.tictactoe.model.Player;
import com.shopee.sandynie.tictactoe.utilities.StringUtility;
import com.shopee.sandynie.tictactoe.viewmodel.GameViewModel;

public class GameActivity extends AppCompatActivity {

    private static final String TAG = GameActivity.class.getSimpleName();
    private static final String GAME_BEGIN_DIALOG_TAG = "game_begin_dialog_tag";
    private static final String GAME_END_DIALOG_TAG = "game_end_dialog_tag";
    private static final String NO_WINNER = "No one";

    private GameViewModel gameViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        promptForPlayers();
    }

    public void promptForPlayers(){
        GameStartDialog dialog = GameStartDialog.newInstance(this);
        dialog.setCancelable(false);
        dialog.show(getSupportFragmentManager(),GAME_BEGIN_DIALOG_TAG);
    }

    public void onPlayersSet(String player1,String player2){
        initDataBinding(player1,player2);
    }

    private void initDataBinding(String player1,String player2){
        ActivityGameBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_game);
        gameViewModel = ViewModelProviders.of(this).get(GameViewModel.class);
        gameViewModel.init(player1,player2);
        binding.setGameViewModel(gameViewModel);
        setUpOnGameEndListener();

    }

    private void setUpOnGameEndListener(){
        gameViewModel.getWinner().observe(this, new Observer<Player>() {
            @Override
            public void onChanged(@Nullable Player player) {
                OnGameWinnerChanged(player);
            }
        } );

    }

    private void OnGameWinnerChanged(Player winner){
        String winnerName = winner == null || StringUtility.isNullOrEmpty(winner.getName()) ? NO_WINNER : winner.getName();
        GameEndDialog dialog = GameEndDialog.newInstance(this, winnerName);
        dialog.setCancelable(false);
        dialog.show(getSupportFragmentManager(), GAME_END_DIALOG_TAG);

    }
}
