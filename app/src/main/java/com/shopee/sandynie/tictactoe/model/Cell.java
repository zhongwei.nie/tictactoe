package com.shopee.sandynie.tictactoe.model;

import com.shopee.sandynie.tictactoe.utilities.StringUtility;

public class Cell {
    private Player player;

    public Cell(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public boolean isEmpty() {
        return player == null || StringUtility.isNullOrEmpty(player.getValue());
    }

    public boolean isSameTo(Cell other){
        return !isEmpty() && other!= null &&
                !other.isEmpty() &&
                player.getValue().equalsIgnoreCase(other.getPlayer().getValue());
    }
}
