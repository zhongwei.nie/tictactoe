package com.shopee.sandynie.tictactoe.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableArrayMap;
import android.util.Log;

import com.shopee.sandynie.tictactoe.model.Cell;
import com.shopee.sandynie.tictactoe.model.Game;
import com.shopee.sandynie.tictactoe.model.Player;
import com.shopee.sandynie.tictactoe.utilities.StringUtility;

public class GameViewModel extends ViewModel {

    private static final String TAG = GameViewModel.class.getSimpleName();
    public ObservableArrayMap<String,String> cells;
    private Game game;

    public void init(String player1,String player2){
        game = new Game(player1,player2);
        cells = new ObservableArrayMap<>();
    }

    public void onClickedCellAt(int row, int column){
        Log.d(TAG,"row:"+row+",column:"+column);
        if (game.getCell(row,column) == null){
            game.setCell(row,column,new Cell(game.getCurrentPlayer()));
            cells.put(StringUtility.stringFromNumbers(row,column),game.getCurrentPlayer().getValue());
            if (game.hasGameEnded()) {
                Log.d(TAG,"game has end!");
                game.reSet();
            }
            else
                game.switchPlayer();
        }
    }

    public LiveData<Player> getWinner(){
        return game.getWinner();
    }
}
