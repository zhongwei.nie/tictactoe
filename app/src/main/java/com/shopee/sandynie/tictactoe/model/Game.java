package com.shopee.sandynie.tictactoe.model;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

public class Game {

    private static final String TAG = Game.class.getSimpleName();

    private static final int BOARD_SIZE = 3;

    private Player player1;
    private Player player2;

    private Player currentPlayer;
    private Cell[][] cells;

    private MutableLiveData<Player> winner = new MutableLiveData<>();

    public Game(String playerOne, String playerTwo){
        cells = new Cell[BOARD_SIZE][BOARD_SIZE];

        player1 = new Player(playerOne,"x");
        player2 = new Player(playerTwo,"o");
        currentPlayer = player1;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public Cell[][] getCells() {
        return cells;
    }

    public Cell getCell(int row,int column){
        return cells[row][column];
    }

    public void setCell(int row,int column,Cell cell){
        cells[row][column] = cell;
    }

    public void setCells(Cell[][] cells) {
        this.cells = cells;
    }

    public MutableLiveData<Player> getWinner() {
        return winner;
    }

    public void setWinner(MutableLiveData<Player> winner) {
        this.winner = winner;
    }

    public boolean hasGameEnded(){
        if (hasThreeSameHorizontalCells() || hasThreeSameVerticalCells() || hasThreeSameDiagonalCells()) {
            winner.setValue(currentPlayer);
            return true;
        }

        if (isFullSet()) {
            winner.setValue(null);
            return true;
        }

        return false;
    }

    public boolean hasThreeSameHorizontalCells(){
        try {
            for (int i=0;i<BOARD_SIZE;i++){
                if (areEqual(cells[i][0],cells[i][1],cells[i][2]))
                    return true;
            }
            return false;
        }catch (NullPointerException e){
            Log.d(TAG, e.getMessage());
            return false;
        }
    }

    public boolean hasThreeSameVerticalCells(){
        try {
            for (int i=0;i<BOARD_SIZE;i++){
                if (areEqual(cells[0][i],cells[1][i],cells[2][i]))
                    return true;
            }
            return false;
        }catch (NullPointerException e){
            Log.d(TAG, e.getMessage());
            return false;
        }
    }

    public boolean hasThreeSameDiagonalCells() {
        try {
            return areEqual(cells[0][0], cells[1][1], cells[2][2]) ||
                    areEqual(cells[0][2], cells[1][1], cells[2][0]);
        } catch (NullPointerException e) {
            Log.e(TAG, e.getMessage());
            return false;
        }
    }



    public boolean isFullSet(){
        for (Cell[] row : cells){
            for (Cell cell : row){
                if (cell == null || cell.isEmpty())
                    return false;
            }
        }
        return true;
    }

    private boolean areEqual(Cell... cells){
        if (cells == null || cells.length == 0)
            return false;
        for (Cell cell : cells){
            if (cell == null || cell.isEmpty())
                return false;
        }

        Cell comparisonBase = cells[0];
        for (int i=1;i<cells.length;i++){
            if (!comparisonBase.isSameTo(cells[i]))
                return false;
        }
        return true;
    }

    public void switchPlayer(){
        currentPlayer = currentPlayer == player1 ? player2 : player1;
    }

    public void reSet(){
        player1 = null;
        player2 = null;
        currentPlayer = null;
        cells = null;
    }
}
