package com.shopee.sandynie.tictactoe.plugins;

import android.content.Context;

import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.Field;

import dalvik.system.DexClassLoader;
import dalvik.system.PathClassLoader;

public class PluginBase {
    public void loadDex(Context context){

        File dex = context.getDir("dexpath",Context.MODE_PRIVATE);  //这里需要的是 dex 的路径 可以用 File.pathSeparator  隔开
        String optimizeDir = dex.getAbsolutePath() +File.separator+"opt_dex";
        File fopt = new File(optimizeDir);

        DexClassLoader dexClassLoader = new DexClassLoader(dex.getAbsolutePath(),fopt.getAbsolutePath(),null,context.getClassLoader());

        PathClassLoader pathClassLoader = (PathClassLoader)context.getClassLoader();
        try{

            //1.先获取到dexClassLoader里面的DexPathList类型的pathList
            Class myDexClassLoader = Class.forName("dalvik.system.BaseDexClassLoader");
            Field myPathListField = myDexClassLoader.getDeclaredField("pathList");
            myPathListField.setAccessible(true);
            Object myPathListObject = myPathListField.get(dexClassLoader);  // 找到我的pathList对象

            //2.通过DexPathList拿到dexElements对象
            Class myPathListClass = myPathListObject.getClass();
            Field myElementField = myPathListClass.getDeclaredField("dexElements");
            myElementField.setAccessible(true);
            Object myElements = myElementField.get(myPathListObject);


            //3.拿到应用程序使用的类加载器的pathList
            Class baseDexClassLoader = Class.forName("dalvik.system.BaseDexClassLoader");
            Field baseDexPathListField = baseDexClassLoader.getDeclaredField("pathList");
            baseDexPathListField.setAccessible(true);
            Object basePathListObject = baseDexPathListField.get(pathClassLoader);

            //4.获取到系统的dexElements对象
            Class basePathListClass = basePathListObject.getClass();
            Field baseDexElementField = basePathListClass.getDeclaredField("dexElements");
            baseDexElementField.setAccessible(true);
            Object baseElementList = baseDexElementField.get(basePathListObject);

            //6.按着先加入dex包里面elment的规律依次加入所有的element，这样就可以保证classLoader先拿到的是修复包里面的Test类。
            Class<?> singElementClass = baseElementList.getClass().getComponentType();
            int elementLen = Array.getLength(baseElementList);
            int myElementLen = Array.getLength(myElements);
            int newElementLen  = elementLen + myElementLen;
            Object neweElementsArray = Array.newInstance(singElementClass,newElementLen);
            for (int i=0;i<newElementLen;i++){
                if (i < myElementLen){
                    Array.set(neweElementsArray,i,Array.get(myElements,i));
                }else{
                    Array.set(neweElementsArray,i,Array.get(baseElementList,i-myElementLen));
                }
            }

            Field systemField = basePathListObject.getClass().getDeclaredField("dexElements");
            systemField.setAccessible(true);
            systemField.set(basePathListObject,neweElementsArray);

        }catch (ClassNotFoundException ex){

        }catch (NoSuchFieldException ex){

        }catch (IllegalAccessException ex){

        }
    }
}
